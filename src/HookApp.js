import React from 'react'
// import { CounterApp } from './component/01-useState/CounterApp'
import { CounterWithCustomHook } from './component/01-useState/CounterWithCustomHook'

export const HookApp = () => {
    return (
        <div>
            <h1>Counter App</h1>

            <CounterWithCustomHook />
        </div>
    )
}
