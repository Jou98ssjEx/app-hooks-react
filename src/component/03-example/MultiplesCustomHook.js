import React from 'react'

import '../02-simpleForm/style.css'
import { useCounter } from '../hook/useCounter';
import { useFetch } from '../hook/useFetch'

export const MultiplesCustomHook = () => {

const {counter, increment} =useCounter(1);

const resp = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);

const {loading, data} = resp;

const { quote, author } = !!data && data[0];


// console.log(data[0])

    return (
        <>
            <h1>BreakingBad Quote</h1>
            <hr/>
        {
            ( loading )? (

                <div className= 'alert alert-info text-center'>
                    Loading....
                </div>
            )
            :
            (
            <blockquote className='blockquote text-center'>
                <p className='mb-2'>{quote}</p>
                <footer className='blockquote-footer'> {author} </footer>
            </blockquote>

            )
        }

        <button 
            onClick={ increment}
            className='btn btn-outline-success'
        >
            Next
        </button>


        </>
    )
}
