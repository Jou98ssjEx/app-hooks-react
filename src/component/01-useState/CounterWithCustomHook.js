import React from 'react'
import { useCounter } from '../hook/useCounter'
import  './style.css';


export const CounterWithCustomHook = () => {

    const {state, increment, decrement, reset } =  useCounter();

    return (
        <>
            <h1> Counter with Hook: { state } </h1>
            <hr/>

            <button 
                className="btn btn-outline-success ml-2"
                onClick={ () => increment(2)}
            > 
                +1 
            </button>   
            <button 
                className="btn btn-outline-danger ml-2"
                onClick={ () => reset()}
            > 
                Reset 
            </button>   
            <button 
                className="btn btn-outline-primary ml-2"
                onClick={ () => decrement(2)}
            > 
                -1 
            </button>   
        </>
    )
}
