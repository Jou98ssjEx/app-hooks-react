import React, { useState } from 'react'

import  './style.css';
export const CounterApp = () => {
    
    // const [counter, setCounter] = useState(10);
    // const [{ counter1, counter2}, setCounter] = useState({
    //     counter1: 10,
    //     counter2: 20
    // });
    
    // const handleCLick = () => {
    //     setCounter({
    //         counter1: counter1 +1,
    //         counter2
    //     });
    // }

    const [state, setState] = useState({
        counter1: 10,
        counter2:20,
        counter3:30,
        counter4:40
    });

    const {counter1, counter2 } =  state;

    const handleCLick =( )=> {
        setState({
            ...state,
            counter1: counter1+1
        })
    }
    // const handleCLick = () => {
    //     setCounter(({ counter1, counter2 }) =>{
    //         let o = {
    //             counter1: counter1 +1,
    //             counter2
    //         }
    //         return o

    //     });
    // }
    return (
        <>
          <h1 className="mt-2"> Counter1  { counter1 }</h1>  
          <h1 className="mt-2"> Counter2  { counter2 }</h1>  

          <hr></hr>

          <button
            className="btn btn btn-outline-success"
            onClick={ handleCLick }
          >
              +1
          </button>
        </>
    )
}
