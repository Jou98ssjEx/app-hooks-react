import React from 'react'
import { useForm } from '../hook/useForm';
// import { Message } from './Message';
import './style.css'

export const FormWithCustomHook = () => {

    const data ={
        name: '',
        email: '', 
        password: ''
    }

    const [values, hadleChangeInput] = useForm(data);

    const { name, email, password } = values;

   

    // const hadleChangeInput = ( { target } ) => {
    //     setStateFrom( {
    //         ...stateFrom,
    //         [target.name] : target.value
    //     })
    // }
    const handleSumit = ( e )=> {
        e.preventDefault();
        console.log(values);
        // values.name= '';
        // values.email='';
        // values.password= '';
        // useForm(data);
    }

    return (
        <form onSubmit={handleSumit}>
            <h1>Form with Hook</h1>   
            <hr/>
            
            {/* <form className="form-group"> */}

                <div>
                    <input
                        name='name'
                        type='text'
                        placeholder='your name'
                        className='form-control mb-3'
                        autoComplete='off'
                        value={ name }
                        onChange={ hadleChangeInput}
                    />
                </div>
                <div>
                    <input
                        name='email'
                        type='text'
                        placeholder='email@email.com'
                        className='form-control mb-3'
                        autoComplete='off'
                        value={ email }
                        onChange={ hadleChangeInput}
                    />
                </div>

                <div>
                    <input
                        name='password'
                        type='password'
                        placeholder='********'
                        className='form-control mb-3'
                        autoComplete='off'
                        value={ password }
                        onChange={ hadleChangeInput}
                    />
                </div>

                <button 
                    type='submit' 
                    className= 'btn btn-outline-success'
                    >
                    Saved
                </button>

            {/* </form> */}

            {/* { ( name === '123') && <Message/> } */}
        </form>
    )
}
