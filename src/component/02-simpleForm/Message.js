import React, { useEffect, useState } from 'react'

export const Message = () => {

    const [coords, setCoords] = useState({
        x: 0,
        y: 0
    });

    const { x, y } = coords;

    useEffect(() => {

        const mouseEvent = ( {x, y} ) =>{
            // console.log(e.x, e.y);

            const coords = { x, y};

            setCoords( coords)
        }

        window.addEventListener('mousemove', mouseEvent)

        // console.log('Aparece')
        return () => {
            window.removeEventListener('mousemove', mouseEvent )
            // console.log('desaparece')
        }
    }, [])

    return (
        <>
            <h3> Message Coords</h3>
            <p>
                x: { x } || y: {y}
            </p>


        </>
    )
}
