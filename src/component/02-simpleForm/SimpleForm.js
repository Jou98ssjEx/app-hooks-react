import React, { useEffect, useState } from 'react'
import { Message } from './Message';
import './style.css'

export const SimpleForm = () => {

    const [stateFrom, setStateFrom] = useState({
        name: '',
        email: ''
    });

    const { name, email } = stateFrom;

    useEffect( ()=> {
        // console.log('hey una vez')
    }, []);

    useEffect( ()=> {
        // console.log('todo el form')
    }, [stateFrom]);

    useEffect( ()=> {
        // console.log('email')
    }, [ email]);

    const hadleChangeInput = ( { target } ) => {
        setStateFrom( {
            ...stateFrom,
            [target.name] : target.value
        })
    }

    return (
        <>
            <h1>Simple Form</h1>   
            <hr/>
            
            {/* <form className="form-group"> */}

                <div>
                    <input
                        name='name'
                        type='text'
                        placeholder='your name'
                        className='form-control mb-3'
                        autoComplete='off'
                        value={ name }
                        onChange={ hadleChangeInput}
                    />
                </div>
                <div>
                    <input
                        name='email'
                        type='text'
                        placeholder='email@email.com'
                        className='form-control mb-3'
                        autoComplete='off'
                        value={ email }
                        onChange={ hadleChangeInput}
                    />
                </div>

            {/* </form> */}

            { ( name === '123') && <Message/> }
        </>
    )
}
