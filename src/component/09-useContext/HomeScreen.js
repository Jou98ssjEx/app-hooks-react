import React, { useContext } from 'react'
import { userContext } from './UserContext'

export const HomeScreen = () => {

   const { user } = useContext(userContext)
   console.log(user)
    return (
        <div>
            <h2> HomeScreen </h2>
            <hr/>

            <pre>
                {
                    // JSON.parse(user)
                    JSON.stringify(user, null, 3)
                }
            </pre>
        </div>
    )
}
