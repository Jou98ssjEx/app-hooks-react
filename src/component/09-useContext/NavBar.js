import React from 'react'
import { Link, NavLink } from 'react-router-dom'

export const NavBar = () => {
    return (
        // <nav>
        //     <li>
        //         <a href='/'>Home</a>
        //     </li>
        //     <li>
        //         <a href='/login'>Login</a>
        //     </li>
        //     <li>
        //         <a href='/about'>About</a>
        //     </li>
        // </nav>
        <nav className="navbar navbar-expand-sm navbar-light bg-light">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">Navbar</Link>
             
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink  className="nav-link" aria-current="page" to="/">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/login">Login</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="About">About</NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}
