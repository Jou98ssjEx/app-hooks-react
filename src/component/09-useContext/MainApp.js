import React, { useState } from 'react'
import { AppRouter } from './AppRouter'
import { userContext } from './UserContext'


// import './style.css';
// import '../style.css'



export const MainApp = () => {

    // const user ={
    //     id: 123,
    //     name:'Algo',
    //     email: 'algo@gmail.com'
    // }

    const [user, setUser] = useState({})
    return (
        // <div>
        //     <h2> MainApp </h2>
        //     <hr/>
        // </div>
        <userContext.Provider value ={ { user, setUser} }>
            <AppRouter />
        </userContext.Provider>
    )
}
