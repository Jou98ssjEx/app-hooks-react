import React, { useContext } from 'react'
import { userContext } from './UserContext'

export const LoginScreen = () => {

    const {setUser } = useContext(userContext);

    return (
        <div>
            <h2> LoginScreen </h2>
            <hr/>

            <button
                className='btn btn-outline-primary'
                onClick={ () => setUser({id:123, name: 'Algo', email: 'algo@gmail.com'})}
            >
                Login
            </button>
        </div>
    )
}
