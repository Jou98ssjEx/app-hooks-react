import React, { useContext } from 'react'
import { userContext } from './UserContext'


export const AboutScreen = () => {

    const { user, setUser } = useContext(userContext);

    const handleClick = ( )=>{
        setUser({})
    }
    return (
        <div>
            <h2> AboutScreen </h2>
            <hr/>

            <pre>
                {
                    JSON.stringify(user, null, 3)
                }
            </pre>

            <button
                className='btn btn-outline-warning'
                onClick={ handleClick }
            >
                Logout
            </button>
        </div>
    )
}
