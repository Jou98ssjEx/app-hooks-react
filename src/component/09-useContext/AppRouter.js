import React from 'react'
// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//     // Link
//   } from "react-router-dom";

import {BrowserRouter, Route, Routes } from 'react-router-dom'

import { AboutScreen } from './AboutScreen';
import { HomeScreen } from './HomeScreen';
import { LoginScreen } from './LoginScreen';
import { NavBar } from './NavBar';

export const AppRouter = () => {
    return (

        <BrowserRouter >

            <NavBar />
            <div
                className='mt-3 container'
            >
            <Routes>
                    {/* <Route path="/" element={<App />}>
                        <Route index element={<Home />} />
                        <Route path="teams" element={<Teams />}>
                            <Route path=":teamId" element={<Team />} />
                            <Route path="new" element={<NewTeamForm />} />
                            <Route index element={<LeagueStandings />} />
                        </Route>
                    </Route> */}
                    {/* <Route  path='/' component={ HomeScreen} > 
                    <Route/>
                    <Route  path='/login' component={ LoginScreen} />
                    <Route  path='/about' component={ AboutScreen} /> */}

                    {/* <Route path='/' element={ <HomeScreen/>}>
                        <Route  path='/login' element={ <LoginScreen/>} />
                        <Route  path='/about' element={ <AboutScreen/>} />
                    </Route> */}

                    <Route path='/' element={ <HomeScreen/>} />
                    <Route path='/login' element={ <LoginScreen/>} />
                    <Route path='/about' element={ <AboutScreen/>} />
                    
            </Routes>

            </div>
        </BrowserRouter>


        // <Router>
        //     <div>
        //         <Switch>
        //             <Route  path='/' component={ HomeScreen} />
        //             <Route  path='/login' component={ LoginScreen} />
        //             <Route  path='/about' component={ AboutScreen} />
        //         </Switch>
        //     </div>
        // </Router>
    )
}
