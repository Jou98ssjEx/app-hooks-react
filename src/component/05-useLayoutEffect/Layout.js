import React, { useLayoutEffect, useRef, useState } from 'react'

import './layout.css'
import { useCounter } from '../hook/useCounter';
import { useFetch } from '../hook/useFetch'

export const Layout = () => {

const {counter, increment} =useCounter(1);

const resp = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);

const {data} = resp;

const { quote } = !!data && data[0];


const pTag = useRef();

const [boxSize, setBoxSize] = useState({})

useLayoutEffect(() => {
    // effect
    // return () => {
    //     cleanup
    // };

    setBoxSize((pTag.current.getBoundingClientRect()))
    // console.log((pTag.current.getBoundingClientRect()))
}, [quote])

// console.log(data[0])

    return (
        <>
            <h1>Layout</h1>
            <hr/>
       
            <blockquote className='blockquote text-center'>
                <p  
                    ref={ pTag }
                    className='mb-2'
                >
                    {quote}
                </p>
                
                {/* <footer className='blockquote-footer'> {author} </footer> */}
            </blockquote>

            <pre>
                { JSON.stringify(boxSize, null, 3)}
            </pre>
        <button 
            onClick={ increment}
            className='btn btn-outline-success'
        >
            Next
        </button>

      


        </>
    )
}
