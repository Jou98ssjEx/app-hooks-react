import React from 'react'

// Memo
/**
 * 
 * SOlo si hay cambios en las props se vuelve a renderizar
 */
export const Small = React.memo( ( { values }) => {
    console.log('lo llamo')

    return (
        <small>
            {values}
        </small>
    )
})
