import React, { useCallback, useEffect, useState } from 'react'
import { ShowIncrement } from './ShowIncrement';

import '../style.css'

export const CallbackHook = () => {
    
    const [counter, setCounter] = useState(1);

    // const increment = () =>{
    //     setCounter( counter +1)
    // }

    const increment = useCallback(
        ( num ) => {
            // callback
            setCounter( c => c+ num )
        },
        [setCounter],
    );

    useEffect(() => {
        // effect
        // return () => {
        //     cleanup
        // }
    }, [increment])

    return (
        <>
            <h1>useCallbackHook: {counter }  </h1>   
            <hr/>

            < ShowIncrement increment= { increment} />
        </>
    )
}
