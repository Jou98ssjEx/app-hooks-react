import React, { useState } from 'react'
import { useCounter } from '../hook/useCounter'
import { Small } from './Small'

import '../style.css'

export const Memorize = () => {

    const { counter, increment } = useCounter(1);

    const [show, setShow] = useState(true);

    return (
        <>
            <h1>Counter <Small values={ counter }/> </h1>
            <hr/>

            <button
                className='btn btn-outline-primary'
                onClick={ increment}
            >
                +1
            </button>

            <button
                className='btn btn-outline-danger mr-3'
                onClick= { () => {
                    setShow( !show)
                } }
            >
                Show/Hidden {JSON.stringify(show)}
            </button>
        </>
    )
}

