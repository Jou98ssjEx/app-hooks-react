import React, { useMemo, useState } from 'react'
import { procespPesado } from '../../helpers/procespPesado';
import { useCounter } from '../hook/useCounter'

import '../style.css'

export const MemoHook = () => {

    const { counter, increment } = useCounter(5000);

    const [show, setShow] = useState(true);

    const resul = useMemo(() => procespPesado(counter), [counter]);

    return (
        <>
            <h1>Counter <small> { counter } </small> </h1>
            <hr/>

            <p>{ resul }</p>

            <button
                className='btn btn-outline-primary'
                onClick={ increment}
            >
                +1
            </button>

            <button
                className='btn btn-outline-danger mr-3'
                onClick= { () => {
                    setShow( !show)
                } }
            >
                Show/Hidden {JSON.stringify(show)}
            </button>
        </>
    )
}

