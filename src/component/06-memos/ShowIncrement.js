import React from 'react'

export const ShowIncrement = React.memo( ( { increment }) => {
    console.log('lo llamo')
    return (
        <button
            className='btn btn-outline-primary'
            onClick= { () => {
                increment(5)
            } }
        >
            Increment
        </button>
    )
})
