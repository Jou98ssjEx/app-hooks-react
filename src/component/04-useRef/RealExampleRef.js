import React, { useState } from 'react'

import {MultiplesCustomHook} from '../03-example/MultiplesCustomHook'

import '../style.css';

export const RealExampleRef = () => {

    const [show, setShow] = useState(true);

    return (
        <div>

            <h1> RealExampleRef</h1>
            <hr/>

            <div>
                { show && <MultiplesCustomHook/>}
            </div>
            

            {/* { ( show ) && (
                <div>
                <MultiplesCustomHook/>
                </div>
            )} */}

            <button 
                className='btn btn-outline-primary mt-3'
                onClick={ () => {
                    setShow(!show);
                }}

            >
                Show/Hidden
            </button>

        </div>
    )
}
