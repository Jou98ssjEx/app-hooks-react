import React from 'react'
import { TodoListItems } from './TodoListItems'

export const TodoList = ( { todos, handleDelete, handleToogle } ) => {

    // console.log({info})
    // return (
    //   <table style={style.table} className='informationTable'>
    //     <thead> 
    //       <tr>
    //         <th style={style.tableCell}>First name</th>
    //         <th style={style.tableCell}>Last name</th>
    //         <th style={style.tableCell}>Phone</th>
    //       </tr>
    //     </thead>
  
    //     {
    //       // console.log('information phoneBook');
    //       // console.log()
    //       // console.log(phoneBook)
  
    //       // info.map( m => (
    //       //   console.log(m.userPhone)
    //       //   <tr>
    //       //       <td>
    //       //         {m.userFirstname}
    //       //       </td>
    //       //       <td>
    //       //         {m.userLastname}
    //       //       </td>
    //       //       <td>
    //       //         {m.userPhone}
    //       //       </td>
    //       //   </tr>
    //       // ))
  
    //     }
  
    //     <tbody>
    //       <tr>
    //         <td>
    //           Nombre
    //         </td>
    //         <td>
    //           Apellido
    //         </td>
    //         <td>
    //           09342344234
    //         </td>
    //       </tr>
    //     </tbody> 
    //   </table>
    // );

    return (
            <ul className='list-group list-group-flush'>
                {
                    todos.map( (todos, i) =>
                    ( 
                        <TodoListItems key={todos.id} todos={todos} i = {i} handleDelete={handleDelete} handleToogle={handleToogle} />
                        // <li
                        //         key={todos.id}
                        //         className='list-group-item'
                        //     >
                        //         <p
                        //             className={ `${ todos.done && 'complete' }  text-center text-info`}
                        //             onClick={ ()=> handleToogle(todos.id)}
                        //         > 
                        //         <b> {i+1 }. </b> 
                        //             {todos.activity.toUpperCase()} 
                        //         </p>
                        //         <button
                        //             className='btn btn-outline-danger w-25'
                        //             onClick={ () => {
                        //                 handleDelete(todos.id)
                        //             } }
                        //         >
                        //             Delete
                        //         </button>
                        // </li>
                    ))
                }

            </ul>
    )
}
