import React, { useEffect, useReducer } from 'react'

import './style.css';
import { TodoList } from './TodoList';
import { todoReducer } from './todoReducer';

// const initialState = [{
//     id: new Date().getTime(),
//     activity: 'Aprender react',
//     done: false
// }]


const init = () => {
    // return [{
    //         id: new Date().getTime(),
    //         activity: 'Aprender react',
    //         done: false
    //     }]

        return JSON.parse( localStorage.getItem('todos')) || []
}


export const TodoApp = () => {

    const [todos, dispacth] = useReducer(todoReducer, [], init);
    // console.log(todos)

    // const[{ desc }, hadleChangeInput, reset] =  useForm({desc: ''});

    useEffect(() => {

        localStorage.setItem('todos', JSON.stringify(todos));
        // effect
        // return () => {
        //     cleanup
        // }
    }, [todos]);

    const handleDelete = ( todoId) => {
        console.log(todoId);

        // crear la accion y el dispacth
        const action = {
            type: 'delete',
            payload: todoId
        }
        dispacth(action);
    }

    const handleToogle = (todoId) => {
        console.log(todoId)

        dispacth({
            type: 'toogle',
            payload: todoId
        })
    }

    const handleAdd = ( newTodo) => {
        dispacth({
            type: 'add',
            payload: newTodo
        });
    }

    // const handleSUmit = ( e ) => {
    //     e.preventDefault();

    //     if(desc.trim().length <=1 ){
    //         console.log(('Validacion'))
    //         return
    //     }
    //     // console.log(e.target[0].value);

    //     const newTODO = {
    //         id: new Date().getTime(),
    //         activity: desc,
    //         // activity: e.target[0].value,
    //         done: false
    //     }

    //     const action = {
    //         type: 'add',
    //         payload: newTODO
    //     }
    //     dispacth(action);
    //     reset();
    //     // e.target[0].value = ''

    // }
    return (
        <div>
            <h1>Todo App ( {todos.length} )</h1>
            <hr/>

            <div className='row'>
                {/* <div className='col-7 text-info '> */}
                <div className='col-7 text-info text-center'>
                    <h3>List TODOS</h3>
                    <hr/>

                    < TodoList todos={todos} handleDelete={handleDelete} handleToogle={handleToogle} />
                    {/* <ul className='list-group list-group-flush'>
                        {
                            todos.map( (todos, i) =>
                               ( <li
                                    key={todos.id}
                                    className='list-group-item'
                                >
                                    <p
                                        className={ `${ todos.done && 'complete' }  text-center text-info`}
                                        onClick={ ()=> handleToogle(todos.id)}
                                    > 
                                    <b> {i+1 }. </b> 
                                         {todos.activity.toUpperCase()} 
                                    </p>
                                    <button
                                        className='btn btn-outline-danger w-25'
                                        onClick={ () => {
                                            handleDelete(todos.id)
                                        } }
                                    >
                                        Delete
                                    </button>
                                </li>))
                        }

                    </ul> */}

                </div>

                <div className='col-5 text-success text-center'>

                    < TodoApp handleAdd ={handleAdd} />
                    {/* <h3>Add TODOS</h3>
                    <hr/>

                    <form onSubmit={ handleSUmit}>
                        <input
                            type='text'
                            name='desc'
                            placeholder='Activity'
                            autoComplete='off'
                            className='form-control'
                            value={desc}
                            onChange={ hadleChangeInput }
                        />

                        <button
                            type='submit'
                            className='btn btn-outline-success w-100 mt-2'
                        >
                            Add
                        </button>

                    </form> */}
                </div>
            </div>


        </div>
    )
}
