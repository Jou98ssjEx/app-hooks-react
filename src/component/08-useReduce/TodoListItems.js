import React from 'react'

export const TodoListItems = ({ todos, i, handleDelete, handleToogle}) => {
    return (
        <li
            key={todos.id}
            className='list-group-item'
        >
            <p
                className={ `${ todos.done && 'complete' }  text-center text-info`}
                onClick={ ()=> handleToogle(todos.id)}
            > 
            <b> {i+1 }. </b> 
                {todos.activity.toUpperCase()} 
            </p>
            <button
                className='btn btn-outline-danger w-25'
                onClick={ () => {
                    handleDelete(todos.id)
                } }
            >
                Delete
            </button>
        </li>
    )
}
