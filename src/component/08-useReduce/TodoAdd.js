import React from 'react'
import { useForm } from '../hook/useForm';

export const TodoAdd = ({ handleAdd }) => {

    const[{ desc }, hadleChangeInput, reset] =  useForm({desc: ''});

    const handleSUmit = ( e ) => {
        e.preventDefault();

        if(desc.trim().length <=1 ){
            console.log(('Validacion'))
            return
        }
        // console.log(e.target[0].value);

        const newTODO = {
            id: new Date().getTime(),
            activity: desc,
            // activity: e.target[0].value,
            done: false
        }

        // const action = {
        //     type: 'add',
        //     payload: newTODO
        // }
        // dispacth(action);
        handleAdd( newTODO)
        reset();
        // e.target[0].value = ''

    }

    return (
        <>
             <h3>Add TODOS</h3>
                <hr/>

                <form onSubmit={ handleSUmit}>
                    <input
                        type='text'
                        name='desc'
                        placeholder='Activity'
                        autoComplete='off'
                        className='form-control'
                        value={desc}
                        onChange={ hadleChangeInput }
                    />

                    <button
                        type='submit'
                        className='btn btn-outline-success w-100 mt-2'
                    >
                        Add
                    </button>

                </form>   
        </>
    )
}
