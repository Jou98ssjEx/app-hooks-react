import { useEffect, useRef, useState } from "react"

export const useFetch = ( url ) => {
    // const data = {data:null, error:null, loading: true};
    const [state, setState] = useState({data:null, error:null, loading: true});

    // el componente esta montado
    const isMounted = useRef(true);

    useEffect(() => {
        
        return () => {
            isMounted.current = false
        }
    }, [])


    useEffect(() => {
       
        setState({data:null, error:null, loading: true})

        fetch(url)
            .then( resp => resp.json())
                .then(data => {

                    if( isMounted.current ){

                        setState( {
                            error: null,
                            loading: false,
                            data
                        });
                        // setTimeout(() => {
                            
                        // }, 4000);
                    } else{
                        console.log('Se previno el componente')
                    }

            })
            .catch( ( ) => {
                setState({
                    error: 'Error al obtener info',
                    loading: false,
                    data: null
                })
            })

    }, [url])

    

    return state;
}
