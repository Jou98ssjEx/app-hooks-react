import React from 'react';
import ReactDOM from 'react-dom';
// import { CallbackHook } from './component/06-memos/CallbackHook';
// import { Padre } from './component/07-tarea-memo/Padre';
// import { TodoApp } from './component/08-useReduce/TodoApp';
import { MainApp } from './component/09-useContext/MainApp';
// import { MemoHook } from './component/06-memos/MemoHook';
// import { FormWithCustomHook } from './component/02-simpleForm/FormWithCustomHook';
// import { MultiplesCustomHook } from './component/03-example/MultiplesCustomHook';
// import { FocusScreen } from './component/04-useRef/FocusScreen';
// import { RealExampleRef } from './component/04-useRef/RealExampleRef';
// import { Layout } from './component/05-useLayoutEffect/Layout';
// import { Memorize } from './component/06-memos/Memorize';
// import { SimpleForm } from './component/02-simpleForm/SimpleForm';
// import { HookApp } from './HookApp';


ReactDOM.render(
    <MainApp />,
  document.getElementById('root')
);
