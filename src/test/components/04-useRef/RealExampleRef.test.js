import React from 'react';

import { shallow } from "enzyme"
import { RealExampleRef } from "../../../component/04-useRef/RealExampleRef"

describe('File Real Example Ref', () => {

    const wrapper = shallow( < RealExampleRef /> );
    
    test('should snapshop of component for default', () => {
        

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('MultiplesCustomHook').exists()).toBe(true)
    });

    test('should simate click in component for view <MultiplesCustomHooks />', () => {
        

        wrapper.find('button').simulate('click');

        expect( wrapper.find('MultiplesCustomHook').exists()).toBe(false)
        // console.log(wrapper.html())
    })
    
    
})
