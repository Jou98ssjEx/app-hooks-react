import React from 'react';
import { useCounter } from '../../../component/hook/useCounter';
import { useFetch } from '../../../component/hook/useFetch';

const { shallow } = require("enzyme");
const { MultiplesCustomHook } = require("../../../component/03-example/MultiplesCustomHook")

jest.mock('../../../component/hook/useFetch');
jest.mock('../../../component/hook/useCounter');

describe('file Multiples Custom Hooks', () => {
    
    useCounter.mockReturnValue({
        counter: 10,
        increment: () => {}
    })

    test('should snapshop of compoment for default', () => {
        

        useFetch.mockReturnValue({
            data: null,
            loading: true,
            error: null
        });

        const wrapper = shallow( <MultiplesCustomHook /> );

        expect( wrapper).toMatchSnapshot();
    });

    test('should component with info', () => {
        
        useFetch.mockReturnValue({
            data: [{
                author: 'Joao g',
                quote: ' Una frase'
            }],
            loading:false,
            error: null
        });

        const wrapper = shallow( <MultiplesCustomHook /> );

        expect( wrapper.find('.alert').exists() ).toBe(false)
        expect( wrapper.find('.mb-2').text().trim() ).toBe('Una frase')
        expect( wrapper.find('footer').text().trim() ).toBe('Joao g')

        console.log(wrapper.html())

    })
    
    
})
