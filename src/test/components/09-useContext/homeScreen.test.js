import React from 'react';

import {mount } from 'enzyme'
import { HomeScreen } from '../../../component/09-useContext/HomeScreen';
import { userContext } from '../../../component/09-useContext/UserContext';

describe('file component Home Screen', () => {
    
    const user = {
        name:'algo',
        email: 'algo@gmail.com'
    }

    const wrapper = mount( 
    <userContext.Provider value={{
        user
    }} >

        <HomeScreen />

    </userContext.Provider>
    
    );

    test('should has snapshopt', () => {
        
        expect(wrapper).toMatchSnapshot();
    })
    
})
