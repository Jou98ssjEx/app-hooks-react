import { mount } from 'enzyme';
import React from 'react';
import { AppRouter } from '../../../component/09-useContext/AppRouter';
import { userContext } from '../../../component/09-useContext/UserContext';

describe('file app Router', () => {
    
    const user = {
        id:1,
        name:'algo'
    }

    const wrapper = mount(
        <userContext.Provider value={{
            user
        }}>
            <AppRouter />
        </userContext.Provider>
    )

    test('should show snapshopt', () => {
        
        expect(wrapper).toMatchSnapshot();
    })
    
})
