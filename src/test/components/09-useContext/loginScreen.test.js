import { mount } from 'enzyme';
import React from 'react';
import { LoginScreen } from '../../../component/09-useContext/LoginScreen';
import { userContext } from '../../../component/09-useContext/UserContext';

describe('file component Login Screen', () => {
    const setUser = jest.fn();
    

    const wrapper = mount( 
        <userContext.Provider value={{setUser}} >

            <LoginScreen /> 
        </userContext.Provider>

    );

    test('should has snapshopt', () => {
        
        expect(wrapper).toMatchSnapshot();
    })

    test('should ejectud setUser with arg', () => {
        

        // wrapper.find('button').simulate('click')
        // console.log(wrapper)
        wrapper.find('button').prop('onClick')();

        expect(setUser).toHaveBeenCalledWith({id:123, name: 'Algo', email: 'algo@gmail.com'})
    })
    
    
})
