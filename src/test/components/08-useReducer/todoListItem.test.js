import { shallow } from 'enzyme';
import React from 'react';
import { TodoList } from '../../../component/08-useReduce/TodoList';
import { demoTodos } from '../../fixtures/demoTodo';

describe('File tod list Item', () => {

    const handleDelete =jest.fn();
    const handleToogle =jest.fn();
    
    const wrapper = shallow( < TodoList todos= {demoTodos} handleDelete = {handleDelete} handleToogle ={handleToogle} /> )

    test('should snapshopt', () => {

        expect( wrapper ).toMatchSnapshot();

        
    });

    test('should has two todoListItems', () => {
        
        expect( wrapper.find('TodoListItems').length).toBe(demoTodos.length);

        // que sea una funcion
        expect( wrapper.find('TodoListItems').at(0).prop('handleDelete') ).toEqual( expect.any(Function))
    })
    
    
})
