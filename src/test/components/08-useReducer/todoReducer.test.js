import { todoReducer } from "../../../component/08-useReduce/todoReducer"
import { demoTodos } from "../../fixtures/demoTodo";

describe('file todo Reducer', () => {
    
    test('should return state for default', () => {
        
        const state =  todoReducer( demoTodos, {});

        expect(state ).toEqual(demoTodos);
    });

    test('should add TODO', () => {
        
        const action ={
            type: 'add',
            payload: {
                id: 3,
                activity: 'Learning Firebase',
                done: false
            }
        };

        const state = todoReducer( demoTodos, action);

        expect( state.length ).toBe(3);
        expect( state[2] ).toEqual(action.payload);
        expect( state ).toEqual([ ...demoTodos, action.payload ]);

        // console.log(state.length)
    })

    test('should delete for TODO', () => {
        
        const action ={
            type: 'delete',
            payload: 3
        };

        const state = todoReducer( demoTodos, action);

        expect( state.length).toBe(2);
        // expect ( state )

    })

    test('should toogle for TODO', () => {

        const action ={
            type: 'toogle',
            payload: 2
        };

        const state = todoReducer( demoTodos, action);

        expect( state[1].done).toBe(true);
        // console.log(state[1].done)
    })
    
    
    
    
})
