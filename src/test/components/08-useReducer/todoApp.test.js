import { act } from '@testing-library/react';
import { mount, shallow } from 'enzyme';
import React from 'react';
import { demoTodos } from '../../fixtures/demoTodo';


describe('file Todo App', () => {

    const wrapper = shallow(< TodoApp/>);

    Storage.prototype.setItem = jest.fn( ( )=> {} );
    
    test('should snapshopt for component', () => {
        
        expect(wrapper).toMatchSnapshot();
    })

    test('should add an TODO', () => {
        

        const wrapper = mount(< TodoApp/>); // funciina como el shallow, pero con mas poder-info

        act( ( )=> {
            wrapper.find('TodoApp').prop('handleAdd')(demoTodos[0]);
            wrapper.find('TodoApp').prop('handleAdd')(demoTodos[1]);
        } );


        expect(wrapper.find('h1').text().trim() ).toBe(`Todo App ( ${demoTodos.length} )`);
        expect( localStorage.setItem).toHaveBeenCalledTimes(2);
    })

    test('should delete an TODO', () => {
        
        // agregar un todo
        wrapper.find('TodoApp').prop('handleAdd') (demoTodos[0]);
        // elmino
        wrapper.find('TodoList').prop('handleDelete') (demoTodos[0].id);

        expect(wrapper.find('h1').text().trim() ).toBe(`Todo App ( ${demoTodos.length} )`);

    })
    
    
    
})
