import { shallow } from 'enzyme';
import React from 'react';
import { TodoAdd } from '../../../component/08-useReduce/TodoAdd';

describe('fiel Todo Add', () => {
    
    const handleAdd = jest.fn();

    const wrapper = shallow( < TodoAdd handleAdd={handleAdd} />)

    test('should snapshot for component', () => {
        

        expect(wrapper).toMatchSnapshot()
    })

    test('should don´t called handleAdd', () => {
        
        const formSumit = wrapper.find('form').prop('onSubmit');

        formSumit( { preventDefault(){}} );

        expect(handleAdd).toHaveBeenCalledTimes(0);
    })

    test('should called fuction todoAdd', () => {
        
       const v = 'Learning React 1';

       wrapper.find('input').simulate('change', { 
           target:{
               value: v,
               name: 'desc'
           }
       });        
       
       const formSumit = wrapper.find('form').prop('onSubmit');

       formSumit( { preventDefault(){}} );

       expect(handleAdd).toHaveBeenCalledTimes(1);
      

       expect(handleAdd).toBeCalledWith( expect.any(Object));
       expect(handleAdd).toBeCalledWith( {
           activity: v,
           done: false,
           id: expect.any(Number)
       });

       expect( wrapper.find('input').prop('value')).toBe('')
    })
    

    
    
    
})
