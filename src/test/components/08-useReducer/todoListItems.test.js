import React from 'react';

import { shallow } from "enzyme"
import { TodoListItems } from "../../../component/08-useReduce/TodoListItems"
import { demoTodos } from '../../fixtures/demoTodo';

describe('file of todoListItems', () => {

    const handleDelete = jest.fn();
    const handleToogle = jest.fn();

    const wrapper = shallow( <TodoListItems todos={ demoTodos[0] } i={0} handleDelete={handleDelete} handleToogle ={ handleToogle} /> )
    // console.log(wrapper.html())
    
    test('should show suscess', () => {
        
        expect( wrapper ).toMatchSnapshot();
    })

    test('should called the fuction handleDelete', () => {

        wrapper.find('button').simulate('click')

        expect( handleDelete ).toHaveBeenCalledWith( demoTodos[0].id)
        
    })

    test('should called the fuction handleToogle', () => {
        
        wrapper.find('p').simulate('click')

        expect( handleToogle ).toHaveBeenCalledWith( demoTodos[0].id)
    })

    test('should show el texto correctamente', () => {
        
        const p = wrapper.find('p').text();

        expect( p ).toBe( ' 1. LEARNING REACT')
        // console.log(p)

        // mostrar 1. Texto
    })
    

    test('should has className complete', () => {
    
        const todo = demoTodos[0];
        todo.done = true;
        
        const wrapper = shallow( <TodoListItems todos={ todo} i={0}  /> )

        expect( wrapper.find('p').hasClass('complete')).toBe(true)

        
    })
    
    
    
})
