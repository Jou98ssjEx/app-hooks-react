import { renderHook, act } from "@testing-library/react-hooks"
import { useForm } from "../../component/hook/useForm"


describe('File of useForm', () => {
    
    const initialForm = {
        name: 'algo',
        email: 'algo@gmail.com'
    }

    test('should return useFrom []', () => {
        
        const { result } = renderHook( ( ) => useForm());

        expect(result.current[0]).toEqual({})
        expect(typeof result.current[1]).toBe('function')
        expect(typeof result.current[2]).toBe('function')

    })
    

    test('should return formulario por defecto', () => {

        const { result } = renderHook( ( ) => useForm());

        expect(result.current[0]).toEqual({})
        // console.log(result.current)
        
    })

    test('should return formulario with values', () => {

        const { result } = renderHook( ( ) => useForm( initialForm));

        expect(result.current[0]).toEqual(initialForm)
        // console.log(result.current)
        
    })

    test('should return formulario change name', () => {

        const { result } = renderHook( ( ) => useForm( initialForm));

        const [ , hadleChangeInput, ] = result.current;

        const newForm = {
            target:{
                name: 'name',
                value: 'algo123'
            },
            // target:{
            //     name: 'email',
            //     value: 'algo123@gmail.com'
            // }
        }

        act( ( ) => {

            // hadleChangeInput( { target: {
            //     name: 'name',
            //     value: 'algo123',
            //     // name: 'email',
            //     // value: 'algo123@gmail.com',

            // }});
            hadleChangeInput(newForm)
        })

        expect(result.current[0] ).toEqual( { name: 'algo123', email: 'algo@gmail.com' })

        // console.log(result.current[0])
        
    })

    test('should re-start form with RESET ', () => {
        
        const { result } = renderHook( ( ) => useForm( initialForm));

        const [ form , hadleChangeInput, reset ] = result.current;

        const newForm = {
            target:{
                name: 'name',
                value: 'algo123'
            },
        }

        act( ( ) => {
            hadleChangeInput(newForm)
            reset()
        })

        expect(form).toEqual(initialForm)


    })
    
    
})
