import { renderHook } from "@testing-library/react-hooks"
import { useFetch } from "../../component/hook/useFetch"


describe('file to use Fetch', () => {
    

    test('should values for default', () => {
        
        const { result  } = renderHook( ( ) => useFetch('https://www.breakingbadapi.com/api/quotes/1') );

        expect(result.current).toEqual({data:null, error:null, loading: true})
        // expect(result.current[0]).toEqual({data:null, error:null, loading: true})
    })

    test('should value of API', async() => {
        const { result, waitForNextUpdate } = renderHook( ( ) => useFetch('https://www.breakingbadapi.com/api/quotes/1') );
        await waitForNextUpdate();
        
        const { data, loading, error } = result.current

        expect(data.length).toBe(1);
        expect( loading ).toBe(false);
        expect( error ).toBe(null)
        // console.log(data.length)
        // expect(result.current).toEqual({data:null, error:null, loading: true})
    })

    test('should maneger error', async() => {
        
        const { result, waitForNextUpdate} = renderHook( ( ) => useFetch('https://reqres.in/apid/users?page=2'))
        await waitForNextUpdate();

        const { data, loading, error} = result.current;

        expect(data).toBe(null);
        expect( loading ).toBe(false);
        expect( error ).toBe('Error al obtener info')

    })
    
    
    
})
