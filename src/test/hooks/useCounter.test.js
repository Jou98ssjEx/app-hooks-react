import { renderHook, act } from '@testing-library/react-hooks'
import { useCounter } from '../../component/hook/useCounter';


describe('File in Hooks to useCounter', () => {
    
    test('should useCounter ', () => {
        
        const {result } = renderHook(() => useCounter());
    
        expect(result.current.counter).toBe(10);
        expect(typeof result.current.increment).toBe('function');
        expect(typeof result.current.decrement).toBe('function');
        expect(typeof result.current.reset).toBe('function');
    })

    test('should value useCounter', () => {
        
        const {result } = renderHook(() => useCounter(100));
    
        expect(result.current.counter).toBe(100);
    })
    

    test('should increment function', () => {
        
        const { result } = renderHook( ( ) => useCounter());
        const { increment } = result.current;

        // Ejecuatamos la funcion
        act( ( ) => {
            increment(); // por defecto incrementa en uno
        });

        const { counter } = result.current;

        expect( counter ).toBe( 11);
    })
   
    test('should decrement function', () => {
        
        const { result } = renderHook( ( ) => useCounter());
        const { decrement } = result.current;

        // Ejecuatamos la funcion
        act( ( ) => {
            decrement(); // por defecto decrementa en uno
        });

        const { counter } = result.current;

        expect( counter ).toBe( 9);
    })
    
    
    test('should reset function', () => {
        
        const { result } = renderHook( ( ) => useCounter());
        const { decrement, reset} = result.current;

        // Ejecuatamos la funcion
        act( ( ) => {
            decrement(); // por defecto decrementa en uno
            // decrement(); se ejecuta solo una vez no permite ejecutar la funcion 2veces
            reset();
        });

        const { counter } = result.current;

        expect( counter ).toBe( 10);
    })
    
    
})
